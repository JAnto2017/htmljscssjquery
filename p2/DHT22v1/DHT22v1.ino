#include <DHT.h>
#include <DHT_U.h>
//------------------------------------------------------
#define DHTPIN 2
#define DHTTYPE DHT22
DHT dht(DHTPIN,DHTTYPE);
//------------------------------------------------------
void setup() {
  Serial.begin(9600);
  Serial.println("Iniciando...");
  dht.begin();
}
//------------------------------------------------------
void loop() {
  
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);
  //Serial.print("humedad: ");
  //Serial.println(h);
  //Serial.print("Temperatura: ");
  //Serial.println(t);
  //Serial.print("Fahrenhei: ");
  //delay(5100);
  //Serial.println(f);
  Serial.write(h); //envía un byte
  delay(2100);
  Serial.write(t);
  delay(2100);
  Serial.write(f);
  delay(2100);
}
